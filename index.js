$(function () {
    $('#vidBox').VideoPopUp({
        opener: "video-trigger",
        idvideo: "demo",
        pausevideo: true,
        maxweight: "229"
    });
});

const video = document.getElementById('#demo');

// Twice as fast!
video.playbackRate = 2;

// Back to normal
// video.playbackRate = 1